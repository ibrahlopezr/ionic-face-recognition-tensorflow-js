import { Injectable } from '@angular/core';
import { rejects } from 'assert';
import * as faceapi from 'face-api.js';
import { FaceDetectionOptions, SsdMobilenetv1Options } from 'face-api.js';
import { BASE_URL } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class TensorflowService {
  MODEL_URL: string = `${BASE_URL}models/`;

  constructor() {}

  /**
   * Loads models for face recognition
   * @returns {Promise<any>} models
   */
  public async loadSsdMobilenetv1Models(): Promise<any> {
    return await new Promise(async (resolve, reject) => {
      try {
        await faceapi.loadSsdMobilenetv1Model(this.MODEL_URL + '/ssd');
        await faceapi.loadFaceLandmarkModel(this.MODEL_URL);
        await faceapi.loadFaceRecognitionModel(this.MODEL_URL);

        resolve('Models SsdMobilenetv1 loaded correctly.');
      } catch (e) {
        console.log(
          'An error occurred while loading the models:',
          e.toString()
        );
        reject('An error occurred while loading the models: ' + e.toString());
      }
    });
  }

  /**
   * Loads models for face recognition
   * @returns {Promise<[void, void, void]>} models
   */
  public async loadMTCNNModels(): Promise<[void, void, void]> {
    const p1 = faceapi.nets.mtcnn.loadFromUri(
      `${this.MODEL_URL}mtcnn/mtcnn_model-weights_manifest.json`
    );

    const p2 = faceapi.nets.faceLandmark68Net.loadFromUri(this.MODEL_URL);

    const p3 = faceapi.nets.faceRecognitionNet.loadFromUri(this.MODEL_URL);
    //const p3 = faceapi.loadFaceRecognitionModel(this.MODEL_URL);
    return await Promise.all([p1, p2, p3]);
  }

  /**
   * Detects all faces bounding boxes
   * @param input image detected
   * @returns {Promise<any>}  full face descriptions
   */
  public async detectAllFacesBoundingBoxes(
    input: faceapi.TNetInput
  ): Promise<any> {
    return await new Promise(async (resolve, reject) => {
      try {
        /* const options: FaceDetectionOptions = new SsdMobilenetv1Options({
          maxResults: 0,
          minConfidence: 0.6,
        }); */
        let fullFaceDescriptions = await faceapi
          .detectAllFaces(input)
          .withFaceLandmarks()
          .withFaceDescriptors();
        //const dimensions = new faceapi.Dimensions(305, 170);
        /*  fullFaceDescriptions = await faceapi.resizeResults(
          fullFaceDescriptions,
          dimensions
        ); */

        resolve(fullFaceDescriptions);
      } catch (e) {
        console.log(
          'An error occurred while detected all faces:',
          e.toString()
        );
        reject('An error occurred while detected all faces: ' + e.toString());
      }
    });
  }

  /**
   * Draws detections
   * @param canvas HTMLELEMENT
   * @param fullFaceDescriptions
   * @returns {Promise<void>} detections draw
   */
  public async drawDetections(
    canvas: string | HTMLCanvasElement,
    fullFaceDescriptions: faceapi.WithFaceDescriptor<
      faceapi.WithFaceLandmarks<
        {
          detection: faceapi.FaceDetection;
        },
        faceapi.FaceLandmarks68
      >
    >[]
  ): Promise<void> {
    return await new Promise(async (resolve, reject) => {
      try {
        resolve(faceapi.draw.drawDetections(canvas, fullFaceDescriptions));
      } catch (e) {
        console.log('An error occurred while draw detections:', e.toString());
        reject('An error occurred while draw detections: ' + e.toString());
      }
    });
  }

  /**
   * Draws face landmarks
   * @param canvas
   * @param fullFaceDescriptions
   * @returns {Promise<void>} face landmarks
   */
  public async drawFaceLandmarks(
    canvas: string | HTMLCanvasElement,
    fullFaceDescriptions: faceapi.WithFaceDescriptor<
      faceapi.WithFaceLandmarks<
        {
          detection: faceapi.FaceDetection;
        },
        faceapi.FaceLandmarks68
      >
    >[]
  ): Promise<void> {
    return await new Promise(async (resolve, reject) => {
      try {
        resolve(faceapi.draw.drawFaceLandmarks(canvas, fullFaceDescriptions));
      } catch (e) {
        console.log('An error occurred while draw detections:', e.toString());
        reject('An error occurred while draw detections: ' + e.toString());
      }
    });
  }

  public drawFaceLandmarksMTCNN(
    mtcnnResults: faceapi.WithFaceLandmarks<
      { detection: faceapi.FaceDetection },
      faceapi.FaceLandmarks5
    >[],
    selector = 'overlay'
  ) {
    try {
      return new Promise(async (resolve, reject) => {
        await faceapi.draw.drawFaceLandmarks(
          selector,
          mtcnnResults.map((res) => res.landmarks)
        );
        resolve('Success drawFaceLandmark');
      });
    } catch (e) {
      rejects(e);
    }
  }

  public drawDetectionsMTCNN(
    mtcnnResults: faceapi.WithFaceLandmarks<
      { detection: faceapi.FaceDetection },
      faceapi.FaceLandmarks5
    >[],
    selector = 'overlay'
  ) {
    return new Promise(async (resolve, reject) => {
      try {
        await faceapi.draw.drawDetections(
          selector,
          mtcnnResults.map((res) => res.detection)
        );
        resolve('Success drawDetectionws');
      } catch (e) {
        reject(e);
      }
    });
  }

  /**
   * Disposes mtcnn model
   * @returns {Promise<void>} mtcnn
   */
  public async disposeMTCNN(): Promise<void> {
    return await new Promise(async (resolve, reject) => {
      try {
        await faceapi.nets.mtcnn.dispose();
        resolve();
      } catch (e) {
        reject(e);
      }
    });
  }
}
