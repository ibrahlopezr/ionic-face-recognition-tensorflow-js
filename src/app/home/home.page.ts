import { TensorflowService } from './../core/services/tensorflow/tensorflow.service';
import { Component, ElementRef, ViewChild } from '@angular/core';
import * as faceapi from 'face-api.js';
import { LoadingController, Platform, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  @ViewChild('inputVideo', { static: true })
  video!: ElementRef<HTMLVideoElement>;
  @ViewChild('overlay', { static: true })
  canvas!: ElementRef<HTMLCanvasElement>;
  @ViewChild('container', { static: true })
  container!: ElementRef<HTMLCanvasElement>;
  imageElement!: ElementRef<HTMLImageElement>;
  fullFaceDescriptions: any;
  faceMatcher: faceapi.FaceMatcher;
  descriptors = { desc1: null, desc2: null };
  threshold: number = 0.6;
  dimension: {
    width: number;
    height: number;
  } = { width: 0, height: 0 };
  constructor(
    private tensorFlow: TensorflowService,
    private loadingCtrl: LoadingController,
    private toastController: ToastController,
    private platform: Platform
  ) {}

  async ngOnInit(): Promise<void> {
    //await this.tensorFlow.disposeMTCNN();
    console.log('await this.initializeApp();');
    this.platform.ready().then(async () => {
      console.log('Width: ' + this.platform.width());
      console.log('Height: ' + this.platform.height());
      this.dimension.width = this.platform.width();
      this.dimension.height = this.platform.height();
      await this.initializeApp();
    });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    // this.setElementFullScreen();
  }

  public async run() {
    try {
      console.log('run');

      var width = this.dimension.width;
      var height = this.dimension.height;
      const constraints = {
        video: {
          width: { min: width },
          height: { min: height },
        },
      };
      this.video.nativeElement.width = width;
      this.video.nativeElement.height = height;

      this.canvas.nativeElement.width = width;
      this.canvas.nativeElement.height = height;
      const { nativeElement: video } = this.video;

      navigator.mediaDevices
        .getUserMedia(constraints)
        .then((stream) => {
          video.srcObject = stream;
        })
        .catch((err) => console.error(err));
      /* navigator.getUserMedia(
        { video: {} },
        (stream) => (this.video.nativeElement.srcObject = stream),
        (err) => console.error(err)
      ); */
      // video.addEventListener('', () => {});

      video.addEventListener(
        'loadeddata',
        () => {
          this.setElementFullScreen();
        },
        false
      );
    } catch (e) {
      throw new Error('An error occurred while run initial functions: ' + e);
    }
  }
  setElementFullScreen() {
    const { nativeElement: video } = this.video;
    // Get window size
    var maxWidth = window.innerWidth;
    var maxHeight = window.innerHeight;
    // Get video size
    var videoWidth = video.videoWidth;
    var videoHeight = video.videoHeight;
    // Calcul relative size
    if (maxWidth > maxHeight) {
      var relatifWidth = videoWidth / (videoHeight / maxHeight);
      var relatifHeight = maxHeight;
    } else {
      var relatifWidth = maxWidth;
      var relatifHeight = videoHeight / (videoWidth / maxWidth);
    }
    // Check if result size is not bigger than canvas size
    if (relatifWidth > maxWidth) {
      relatifWidth = maxWidth;
      relatifHeight = (relatifWidth * videoHeight) / videoWidth;
    } else if (relatifHeight > maxHeight) {
      relatifHeight = maxHeight;
      relatifWidth = (relatifHeight * videoWidth) / videoHeight;
    }
    // Set size from window
    video.height = relatifHeight;
    video.width = relatifWidth;
  }

  async presentLoading(message: string) {
    const loading = await this.loadingCtrl.create({
      message: message,
      spinner: 'bubbles',
    });
    await loading.present();
  }

  /**
   * Determines whether play on
   * @returns {Promise<void>} play
   */
  public async onPlay(): Promise<void> {
    try {
      //#region  Constantes
      const paramsOptions: faceapi.FaceDetectionOptions = new faceapi.MtcnnOptions(
        { minFaceSize: 100 }
      );
      const { nativeElement: video } = this.video;
      const { nativeElement: canvas } = this.canvas;
      video.height = this.dimension.height;
      video.width = this.dimension.width;

      // canvas.height  =   this.dimension.height;
      // canvas.width   =   this.dimension.width;
      const displaySize = {
        width: video.width *1.2,
        height: video.height *1.5,
      };
      //#endregion
      /* const ctx = canvas.getContext('2d');

      ctx.drawImage(video, 0, 0, canvas.width, canvas.height); */

      //Get labels face desriptors for compare with face detected.
      const labeledFaceDescriptors = await this.getlabeledFaceDescriptors(
        paramsOptions
      );

      //init face matcher
      const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, 0.6);

      //get single face detected with camera
      const detection = paramsOptions
        ? await faceapi
            .detectSingleFace(this.video.nativeElement, paramsOptions)
            .withFaceLandmarks()
            .withFaceDescriptor()
        : await faceapi
            .detectSingleFace(this.video.nativeElement)
            .withFaceLandmarks()
            .withFaceDescriptor();

      // resize result for draw in canvas
      const resizedDetection = faceapi.resizeResults(detection, displaySize);

      if (detection) {
        const bestMatch = faceMatcher.findBestMatch(detection.descriptor);
        //console.log(bestMatch.toString());
        //#region print face landmarks

        canvas.width = displaySize.width;
        canvas.height = displaySize.height;
        faceapi.draw.drawFaceLandmarks(canvas, resizedDetection);
        const box = resizedDetection.detection.box;
        const drawBox = new faceapi.draw.DrawBox(box, {
          label: bestMatch.toString(),
        });
        drawBox.draw(canvas);
        //#endregion en print face landmarks
        //this.presentToast(bestMatch.toString());
      }

      setTimeout(() => this.onPlay(), 1);
    } catch (e) {
      console.log('An error occurred while on play video: ' + e);
      setTimeout(() => this.onPlay(), 1);
    }
  }

  private async getlabeledFaceDescriptors(
    paramsOptions: faceapi.FaceDetectionOptions
  ) {
    const labels = ['messi', 'selena', 'theweeknd', 'ibrahim'];

    return await Promise.all(
      labels.map(async (label) => {
        // fetch image data from urls and convert blob to HTMLImage element
        const imgUrl = `assets/images/${label}.jpg`;
        const img = await faceapi.fetchImage(imgUrl);

        // detect the face with the highest score in the image and compute it's landmarks and face descriptor
        const fullFaceDescription = paramsOptions
          ? await faceapi
              .detectSingleFace(img, paramsOptions)
              .withFaceLandmarks()
              .withFaceDescriptor()
          : await faceapi
              .detectSingleFace(img)
              .withFaceLandmarks()
              .withFaceDescriptor();

        if (!fullFaceDescription) {
          throw new Error(`no faces detected for ${label}`);
        }

        const faceDescriptors = [fullFaceDescription.descriptor];
        return new faceapi.LabeledFaceDescriptors(label, faceDescriptors);
      })
    );
  }

  private async initializeApp() {
    try {
      console.log('after initialize');

      this.tensorFlow
        .loadMTCNNModels()
        .then(async (res) => {
          console.log('loadMTCNNModels', res);
          await this.run();
        })
        .catch(async (e) => {
          console.log(e);
          // await this.tensorFlow.disposeMTCNN();
          // await this.initializeApp();
        });
    } catch (e) {
      console.log('Error constructor app component:', e.message);
      await this.tensorFlow.disposeMTCNN();
      await this.initializeApp();
    }
  }

  async ngOnDistroy(): Promise<void> {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
  }

  public async computingFaceDescriptors(
    results: faceapi.WithFaceDescriptor<
      faceapi.WithFaceLandmarks<
        {
          detection: faceapi.FaceDetection;
        },
        faceapi.FaceLandmarks68
      >
    >[]
  ) {
    const alignedFaceBoxes = results.map(({ landmarks: faceLandmarks }) =>
      faceLandmarks.align()
    );
    let input: any;
    const alignedFaceTensors = await faceapi.extractFaceTensors(
      input,
      alignedFaceBoxes
    );

    // free memory
    alignedFaceTensors.forEach((t) => t.dispose());
  }
}
